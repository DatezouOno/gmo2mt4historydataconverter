@echo off
REM cd FXneo
REM del USDJPYM1.csv
REM del EURJPYM1.csv
REM del EURUSDM1.csv
REM del GBPJPYM1.csv
REM del GBPUSDM1.csv
REM gmo2mt4csv.bat
REM cd ..
cd ..\FXneoPlus
del USDJPYM1.csv
del EURJPYM1.csv
del EURUSDM1.csv
del GBPJPYM1.csv
del GBPUSDM1.csv
gmo2mt4csvPlus.bat
cd ..
copy FXneo\USDJPYM1.csv + FXneoPlus\USDJPYM1.csv USDJPYM1.csv
copy FXneo\EURJPYM1.csv + FXneoPlus\EURJPYM1.csv EURJPYM1.csv
copy FXneo\EURUSDM1.csv + FXneoPlus\EURUSDM1.csv EURUSDM1.csv
copy FXneo\GBPJPYM1.csv + FXneoPlus\GBPJPYM1.csv GBPJPYM1.csv
copy FXneo\GBPUSDM1.csv + FXneoPlus\GBPUSDM1.csv GBPUSDM1.csv
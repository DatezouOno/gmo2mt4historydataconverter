@echo off
@setlocal enabledelayedexpansion

@for /f "usebackq" %%f in (`dir /s /b EURUSD_*.csv`) do (
  @echo Processing %%f...
  @for /f "skip=1 tokens=1,2,3,4,5,6,7,8,9 delims=," %%i in (%%f) do (
    @set timestamp=%%i
    @echo !timestamp:~0,4!.!timestamp:~5,2!.!timestamp:~8,2!,!timestamp:~11,5!,%%j,%%k,%%l,%%m >> EURUSDM1.csv
  )
)

@for /f "usebackq" %%f in (`dir /s /b USDJPY_*.csv`) do (
  @echo Processing %%f...
  @for /f "skip=1 tokens=1,2,3,4,5,6,7,8,9 delims=," %%i in (%%f) do (
    @set timestamp=%%i
    @echo !timestamp:~0,4!.!timestamp:~5,2!.!timestamp:~8,2!,!timestamp:~11,5!,%%j,%%k,%%l,%%m >> USDJPYM1.csv
  )
)

@for /f "usebackq" %%f in (`dir /s /b EURJPY_*.csv`) do (
  @echo Processing %%f...
  @for /f "skip=1 tokens=1,2,3,4,5,6,7,8,9 delims=," %%i in (%%f) do (
    @set timestamp=%%i
    @echo !timestamp:~0,4!.!timestamp:~5,2!.!timestamp:~8,2!,!timestamp:~11,5!,%%j,%%k,%%l,%%m >> EURJPYM1.csv
  )
)

@for /f "usebackq" %%f in (`dir /s /b GBPJPY_*.csv`) do (
  @echo Processing %%f...
  @for /f "skip=1 tokens=1,2,3,4,5,6,7,8,9 delims=," %%i in (%%f) do (
    @set timestamp=%%i
    @echo !timestamp:~0,4!.!timestamp:~5,2!.!timestamp:~8,2!,!timestamp:~11,5!,%%j,%%k,%%l,%%m >> GBPJPYM1.csv
  )
)
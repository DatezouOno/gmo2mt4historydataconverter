@echo off
@setlocal enabledelayedexpansion

@for /f "usebackq" %%f in (`dir /s /b EURUSD_*.csv`) do (
  @echo Processing %%f...
  @for /f "skip=1 tokens=1,2,3,4,5,6,7 delims=," %%i in (%%f) do (
    @set timestamp=%%i
@rem    @echo !timestamp:~0,4!,!timestamp:~4,2!/!timestamp:~6,2!,!timestamp:~8,2!:!timestamp:~10,2!,%%j,%%k,%%l,%%m,%%n,%%o >> EURUSD_M1.csv
    @echo !timestamp:~0,4!.!timestamp:~4,2!.!timestamp:~6,2!,!timestamp:~8,2!:!timestamp:~10,2!,%%j,%%k,%%l,%%m >> EURUSDM1.csv
  )
)

@for /f "usebackq" %%f in (`dir /s /b USDJPY_*.csv`) do (
  @echo Processing %%f...
  @for /f "skip=1 tokens=1,2,3,4,5,6,7 delims=," %%i in (%%f) do (
    @set timestamp=%%i
@rem    @echo !timestamp:~0,4!,!timestamp:~4,2!/!timestamp:~6,2!,!timestamp:~8,2!:!timestamp:~10,2!,%%j,%%k,%%l,%%m,%%n,%%o >> USDJPY_M1.csv
    @echo !timestamp:~0,4!.!timestamp:~4,2!.!timestamp:~6,2!,!timestamp:~8,2!:!timestamp:~10,2!,%%j,%%k,%%l,%%m >> USDJPYM1.csv
  )
)